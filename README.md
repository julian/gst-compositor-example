# gst-compositor-example

Example of a gstreamer pipeline that composes 2 input file sources. The
resulting video resolution is 720p, where the first input fits the whole video
size, and the second input only fits the top right corner:

    .------------------.
    |         | Input1 |
    |         '--------|
    |                  |
    |     Input0       |
    '------------------'

The gstreamer pipeline graph looks like this:

    filesrc0 -> decodebin0 ->
                              compositor -> capsfilter -> videoconvert -> ximagesink
    filesrc1 -> decodebin1 ->

## Building

This example uses *Meson* as build system. Similar to CMake and Autotools, Meson
generates build files for a lower level build tool called *Ninja*, working in
about the same level of abstraction as more familiar GNU make does.

Meson uses a user-specific build directory and all files produces by Meson are
in that build directory. This build directory will be called `build` in this
document.

The only dependency to build this example is gstreamer >= 1.18.5

Generate the build files:

```
$ meson setup build
```

Configure the project:

```
$ meson configure build
```

Finally, invoke the build:

```
$ ninja -C build
```

## Running

If the build was successful, the example executable should be located under
the `./build/src/` directory. You can run it as a normal Linux executable with
dot slash:

```
$ ./build/src/gst-compositor-example input0.mp4 input1.mp4
```

If you run the example executable without input args, default video frames will
be generated and used.


## Installation

You can install the bridges in your system with:

```
$ sudo meson install
```

## Uninstallation

To uninstall the bridges from your system, use:

```
$ sudo meson uninstall
