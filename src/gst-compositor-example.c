#include <stdio.h>
#include <gst/gst.h>

#define OUTPUT_WIDTH 1280
#define OUTPUT_HEIGHT 720

static gboolean
bus_call (GstBus *bus, GstMessage *msg, gpointer data)
{
  GMainLoop *loop = (GMainLoop *)data;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:
      printf ("End of stream received\n");
      g_main_loop_quit (loop);
      break;
    case GST_MESSAGE_ERROR: {
      GError *err;
      gst_message_parse_error (msg, &err, NULL);
      printf ("Error received: %s\n", err->message);
      g_error_free (err);
      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}

static void
on_pad_added (GstElement *decoder, GstPad *pad, gpointer data)
{
  GstElement *compositor = (GstElement *)data;
  GstCaps *accepted_caps, *current_caps;

  /* only link video pad */
  accepted_caps = gst_caps_new_simple ("video/x-raw", NULL, NULL);
  current_caps = gst_pad_get_current_caps (pad);
  if (gst_caps_can_intersect (accepted_caps, current_caps)) {
    GstPadTemplate *pad_template = gst_element_class_get_pad_template (
        GST_ELEMENT_CLASS (G_OBJECT_GET_CLASS (compositor)), "sink_%u");
    GstPad *sinkpad = gst_element_request_pad (compositor, pad_template, NULL, NULL);

    /*
     * We want Input1 to be on the top right corner, over Input0.
     *
     * .------------------.
     * |         | Input1 |
     * |         '--------|
     * |                  |
     * |     Input0       |
     * '------------------'
     */
    {
      gchar *name = gst_element_get_name (decoder);
      if (strcmp (name, "decoder0") == 0) {
        g_object_set (G_OBJECT (sinkpad), "width", OUTPUT_WIDTH, NULL);
        g_object_set (G_OBJECT (sinkpad), "height", OUTPUT_HEIGHT, NULL);
        g_object_set (G_OBJECT (sinkpad), "xpos", 0, NULL);
        g_object_set (G_OBJECT (sinkpad), "ypos", 0, NULL);
        g_object_set (G_OBJECT (sinkpad), "zorder", 0, NULL);
      } else {
        g_object_set (G_OBJECT (sinkpad), "width", OUTPUT_WIDTH / 2, NULL);
        g_object_set (G_OBJECT (sinkpad), "height", OUTPUT_HEIGHT / 2, NULL);
        g_object_set (G_OBJECT (sinkpad), "xpos", OUTPUT_WIDTH / 2, NULL);
        g_object_set (G_OBJECT (sinkpad), "ypos", 0, NULL);
        g_object_set (G_OBJECT (sinkpad), "zorder", 1, NULL);
      }
      g_free (name);
    }

    gst_pad_link (pad, sinkpad);
    gst_object_unref (sinkpad);
  }

  gst_caps_unref (accepted_caps);
  gst_caps_unref (current_caps);
}

static gboolean
configure_pipeline (GstElement *pipeline, const gchar *path0,
    const gchar *path1)
{
  GstElement *source[2], *decoder[2], *compositor, *converter, *capsfilter,
      *sink;

  /* create the pipeline elements */
  source[0] = gst_element_factory_make (path0 ? "filesrc" : "videotestsrc",
      "source0");
  decoder[0] = gst_element_factory_make ("decodebin", "decoder0");
  source[1] = gst_element_factory_make (path1 ? "filesrc" : "videotestsrc",
      "source1");
  decoder[1] = gst_element_factory_make ("decodebin", "decoder1");
  compositor = gst_element_factory_make ("compositor", "compositor");
  capsfilter = gst_element_factory_make ("capsfilter", "capsfilter");
  converter = gst_element_factory_make ("videoconvert", "converter");
  sink = gst_element_factory_make ("ximagesink", "sink");
  if (!source[0] || !source[0] || !decoder[1] || !decoder[1] || !compositor ||
      !converter || !sink) {
    gst_clear_object (&source[0]);
    gst_clear_object (&source[0]);
    gst_clear_object (&decoder[1]);
    gst_clear_object (&decoder[1]);
    gst_clear_object (&compositor);
    gst_clear_object (&capsfilter);
    gst_clear_object (&converter);
    gst_clear_object (&sink);
    return FALSE;
  }

  /* configure the source */
  if (path0)
    g_object_set (G_OBJECT (source[0]), "location", path0, NULL);
  else
    g_object_set (G_OBJECT (source[0]), "pattern", 0, NULL);
  if (path1)
    g_object_set (G_OBJECT (source[1]), "location", path1, NULL);
  else
    g_object_set (G_OBJECT (source[1]), "pattern", 18, NULL);

  /* configure capsfilter (output resolution) */
  {
    GstCaps *caps = gst_caps_new_simple ("video/x-raw",
        "width", G_TYPE_INT, (gint) OUTPUT_WIDTH,
        "height", G_TYPE_INT, (gint) OUTPUT_HEIGHT,
        NULL);
    g_object_set (G_OBJECT (capsfilter), "caps", caps, NULL);
    gst_caps_unref (caps);
  }

  /* add elements into the pipeline */
  gst_bin_add_many (GST_BIN (pipeline), source[0], decoder[0], source[1],
      decoder[1], compositor, capsfilter, converter, sink, NULL);

  /* link elements */
  if (!gst_element_link (source[0], decoder[0]))
    return FALSE;
  if (!gst_element_link (source[1], decoder[1]))
    return FALSE;
  g_signal_connect (decoder[0], "pad-added", G_CALLBACK (on_pad_added), compositor);
  g_signal_connect (decoder[1], "pad-added", G_CALLBACK (on_pad_added), compositor);
  if (!gst_element_link_many (compositor, capsfilter, converter, sink, NULL))
    return FALSE;

  return TRUE;
}

int
main (int argc, char *argv[])
{
  gint res = 0;
  GMainLoop *loop;
  GstElement *pipeline;
  const gchar *path0 = NULL;
  const gchar *path1 = NULL;

  /* init gstreamer */
  gst_init (NULL, NULL);

  /* get arguments */
  if (argc >= 2) {
    path0 = argv[1];
    if (argc >= 3)
      path1 = argv[2];
  }

  /* create main loop */
  loop = g_main_loop_new (NULL, FALSE);

  /* create and configure pipeline */
  pipeline = gst_pipeline_new ("compositor-pipeline");
  if (!configure_pipeline (pipeline, path0, path1)) {
    res = -1;
    goto done;
  }

  /* add a message handler */
  {
    GstBus *bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
    gst_bus_add_watch (bus, bus_call, loop);
    gst_object_unref (bus);
  }

  /* play pipeline and run main loop */
  gst_element_set_state (pipeline, GST_STATE_PLAYING);
  g_main_loop_run (loop);

  /* stop pipeline when main loop quits */
  gst_element_set_state (pipeline, GST_STATE_NULL);
  res = 0;

done:
  gst_clear_object (&pipeline);
  g_clear_pointer (&loop, g_main_loop_unref);
  return res;
}
